package com.realogy.decom

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import re.anywhere.decom.R
import android.text.method.LinkMovementMethod
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val hyperlink: TextView = findViewById(R.id.tv_browse_message)
        hyperlink.movementMethod = LinkMovementMethod.getInstance()
    }
}